#!/bin/bash

BAM=$1 # Indexing required, 'samtools index ${BAM}'

if [[ ! -f ${BAM} ]]; then
  echo "Invalid BAM: '${BAM}'"
  exit 1
fi

VCF="" # Indexing required, 'gatk IndexFeatureFile -I ${VCF}'
REF="" # Indexing required, 'bwa index ${REF}; samtools index ${REF}'
BED=""

sample="$(basename ${BAM} | cut -d'.' -f1)"
PILUEP_OUTPUT="${sample}_pileups.table"

CMD="gatk GetPileupSummaries "
CMD+="-I ${BAM} "
CMD+="-O ${PILUEP_OUTPUT} "
CMD+="-V ${VCF} "
CMD+="-L ${BED} "
CMD+="-R ${REF} "

P_LOG="log_${sample}_GetPileupSummaries.out"
echo ${CMD}
eval ${CMD} > ${P_LOG} 2>&1

if [[ ! -f ${PILUEP_OUTPUT} ]]; then
  echo "[FAILED] pileups (${P_LOG})"
  echo ""
  exit 1
fi

CONTAMINATION_OUTPUT="${sample}_contamination.table"
CMD="gatk CalculateContamination "
CMD+="-I ${PILUEP_OUTPUT} "
CMD+="-O ${CONTAMINATION_OUTPUT}"

C_LOG="log_${sample}_CalculateContamination.out"
echo ${CMD}
eval ${CMD} > ${C_LOG} 2>&1

if [[ ! -f ${CONTAMINATION_OUTPUT} ]]; then
  echo "[FAILED] pileups (${C_LOG})"
  echo ""
  exit 1
fi

echo "[SUCCESS] (${CONTAMINATION_OUTPUT})"
echo ""
